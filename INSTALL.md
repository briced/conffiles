#!/bin/bash

# Let's go
sudo apt-get update
sudo apt-get upgrade

# Base utils
sudo apt-get install -y libssl-dev openssl htop vim

# dev mode engaged
sudo apt-get install -y firefox git

# Dropbox
# https://www.dropbox.com/install?os=lnx

# Spideroak
# https://spideroak.com/opendownload/

# Tresorit
# https://tresorit.com/download/linux

# Sublime Text 3
# http://www.sublimetext.com/3
# + Package control
# https://packagecontrol.io/

# Utils
sudo apt-get install -y xclip

# After setting github, let's get our projects home
cd ~
mkdir workspace
cd workspace
git clone git@git.framasoft.org:briced/website.git
git clone git@git.framasoft.org:briced/conffiles.git
git clone git@git.framasoft.org:briced/webautomates.git
git clone git@github.com:briced/leboncoin-api.git
git clone git@github.com:briced/achat-citoyen.git


# Python libs
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm python-xlib python3-xlib python-tk tk-dev libx11-dev

# Dev mode strikes back, we need nvm
# https://github.com/creationix/nvm
# And pyenv
# https://github.com/yyuu/pyenv-installer
# And Git-prompt
# https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh


# We're doing JS!
npm i -g grunt
npm i -g grunt-cli
npm i -g gulp

# Also, java
# Download a jdk, then
sudo update-alternatives --install "/usr/bin/java" "java" "/home/ubuntu/dev/jdk1.8.0_25/bin/java" 1
sudo update-alternatives --install "/usr/bin/javac" "javac" "/home/ubuntu/dev/jdk1.8.0_25/bin/javac" 1
sudo chmod a+x /usr/bin/java
sudo chmod a+x /usr/bin/javac
sudo chown -R root:root /home/ubuntu/dev/jdk1.8.0_25/

# Zeal for offline docs
# https://zealdocs.org/

# Install eg
pyenv install 2.7.9
pip install eg

# Let's clean
sudo apt-get autoremove