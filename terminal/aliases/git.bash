# Git
# https://github.com/git/git/blob/master/contrib/completion/git-completion.bash
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi

# https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh
if [ -f ~/git-prompt.sh ]; then
	source ~/git-prompt.sh
	COLOR_BLUE_BOLD="\[\033[1;34m\]"
	COLOR_END="\[\033[0m\]"
	PROMPT_GIT="\$(__git_ps1 '(%s)')"
	GIT_PS1_SHOWSTASHSTATE=1
    GIT_PS1_SHOWUNTRACKEDFILES=1
    GIT_PS1_SHOWUPSTREAM=verbose
    PS1="\u@\W $COLOR_BLUE_BOLD$PROMPT_GIT$COLOR_END ~ "
fi

# shortcuts
gs() {
    git status
}

#Checkout
gck() {
    BRANCH=${1:-""}
    git checkout "$BRANCH"
}
gckb() {
    BRANCH=${1:-""}
    git checkout -b "$BRANCH"
}
gckf() {
    CHECKOUTED_FILE=${1:-"."}
    git checkout -- "$CHECKOUTED_FILE"
}

# Add
ga() {
    ADDED_FILE=${1:-"."}
    git add "$ADDED_FILE"
}
gca() {
    COMMIT_MESSAGE=${1:-"."}
    git commit -am "$COMMIT_MESSAGE"
}
gcane() {
    ADDED_FILE=${1:-"."}
    git add "$ADDED_FILE" && git commit --amend --no-edit
}

# Commit
gcom() {
    COMMIT_MESSAGE=${1:-""}
    git commit -m "$COMMIT_MESSAGE"
}
gcone() {
    git commit --amend --no-edit
}

# Diff
gd() {
    DIFFED_FILE=${1:-"."}
    git diff "$DIFFED_FILE"
}

# Pull & Push
gpl() {
    git pull --rebase
    git pull --tags
}
gps() {
    PARAMS=${1:-""}
    git push $PARAMS
}
gpss() {
    CURRENT_BRANCH=`git branch | awk '/^\*/{print $2}'`
    git push --set-upstream origin $CURRENT_BRANCH
}
grh() {
    RESETED_FILE=${1:-"."}
    git reset HEAD "$RESETED_FILE"
}

# Log
gl() {
    git log
}
glo() {
    git log --oneline
}
# Stash
gss() {
    if [ -z "$1" ]; then 
        git stash push
    else 
        git stash push -m $1
    fi
}
gsp() {
    STASH_TO_POP=${1:-""}
    git stash pop $STASH_TO_POP
}
gsl() {
    git stash list
}
gsc() {
    git stash clear
}

gpurge() {
    # Purge remote branches
    git fetch --prune
    # Purge local branches not tracked on remote
    git branch --merged | grep -v "\*" | grep -v "master" | xargs -n 1 git branch -d
    # Update tags
    git tag | xargs git tag -d && git fetch --tags
    # GC
    git prune
    git gc
}

syncfork() {
    BRANCH=${1:-"master"}
    git fetch upstream
    git checkout $BRANCH
    git merge --ff-only upstream/$BRANCH
}

gogithub() {
     CURRENT_ORGANISATION=$(git config --local remote.origin.url|sed -n 's#.*:\([^/]*\)\/.*#\1#p')
     CURRENT_PROJECT=$(git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p')
     CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
     xdg-open "https://github.com/$CURRENT_ORGANISATION/$CURRENT_PROJECT/compare/$CURRENT_BRANCH" &
}