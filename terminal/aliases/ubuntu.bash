# Débloquer clavier sous IntelliJ 
alias unlock-keyboard='ibus-daemon -rd'
alias fix-clock-ubuntu='killall unity-panel-service'
alias fix-sound='pulseaudio -k && sudo alsa force-reload'
alias foulouupgradou='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y'

firstinstall() {
	sudo add-apt-repository ppa:git-core/ppa --yes
	sudo apt-get update
	sudo apt-get upgrade
}