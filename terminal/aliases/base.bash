alias add='echo "sudo add-apt-repository";sudo add-apt-repository '
alias ppa='echo "sudo add-apt-repository";sudo add-apt-repository '
alias add-remove='echo "sudo add-apt-repository --remove"; sudo add-apt-repository --remove '
alias ppa-remove='echo "sudo add-apt-repository --remove"; sudo add-apt-repository --remove '

alias reboot='echo "sudo reboot"; sudo reboot'
alias update='echo "sudo apt-get update"; sudo apt-get update'
alias install='echo "sudo apt-get install"; sudo apt-get install'
alias upgrade='echo "*** sudo apt-get; sudo apt-get dist-upgrade"; sudo apt-get update; sudo apt-get dist-upgrade; echo "Terminée"'
alias dist-upgrade='echo "*** sudo apt-get; sudo apt-get dist-upgrade"; sudo apt-get update; sudo apt-get dist-upgrade; echo "Terminée"'

alias autoremove='echo "sudo apt-get autoremove --purge"; sudo apt-get autoremove --purge'
alias remove='echo "sudo apt-get autoremove --purge"; sudo apt-get autoremove --purge'

alias ll='ls -al'

set_term_title(){
   echo -en "\033]0;$1\a"
}

alias meteo='curl "http://wttr.in/Paris%20,France"'

k9() {
	ps aux | grep "$1" | awk -F' ' '{print $2}' | xargs kill -9
}

wgetforce() {
	wget --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 "$1"
}

pcinfos() {
	sudo dmidecode -s system-serial-number
}

alias coinbase="date && curl -s 'https://api.coinbase.com/v2/prices/EUR/spot' | jq -r '.data[] | \"\(.base): \(.amount)\"'"