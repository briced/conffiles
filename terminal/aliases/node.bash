# Node
export NVM_DIR="/home/ubuntu/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm


## Auto load nvm when there's a .nvmrc file
OLD_PWD=""
promptCommand() {
    if [ "$OLD_PWD" != "$PWD" ] ;
        then
        OLD_PWD="$PWD"
        if [ -e .nvmrc ] ;
            then nvm use;
        fi
    fi
}
export PROMPT_COMMAND=promptCommand